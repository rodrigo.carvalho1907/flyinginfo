from unittest import TestCase

from src.entities import AirportComm
from src.aptcomms import _pluck_aptcomm

class TestAirportCommunications(TestCase):
    def setUp(self):
        self.line = '''CYOC MUL 122.10   MP 67.570069  -139.839881 21.0 '''

    def test__pluck_aptcomm(self):
        expected = AirportComm(
            ident= "CYOC",
            position= "MUL",
            freq=122.10,
            type=" MP",
            lat=67.570069,
            lng=-139.839881,
            unknow=21.0
        )

        actual = _pluck_aptcomm(self.line)

        self.assertEqual(actual, expected)
        