from unittest import TestCase

from src.entities import Nav
from src.navs import _pluck_nav

class TestNavaids(TestCase):
    def setUp(self):
        self.line = '''1A        WILLIAMS_HARBOUR                        \
            52.559378    -55.781917    NDB 373.00 CAN '''

    def test__pluck_nav(self):
        expected = Nav(
            ident = "1A",
            name = "WILLIAMS_HARBOUR",
            lat = 52.559378,
            lng = -55.781917,
            type = "NDB",
            freq = 373.00,
            loc = "CAN"
        )

        actual = _pluck_nav(self.line)

        self.assertEqual(actual, expected)
