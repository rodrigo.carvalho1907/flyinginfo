from flask import Flask, render_template, request, redirect, url_for
from src.data import AirportCommRepository
from src.create_json import write_airport_freqs

write_airport_freqs("data/aptcomms.txt")

app = Flask(__name__, template_folder="web_templates")
freqs_repo = AirportCommRepository('data/airport_freqs.json')

@app.route('/')
def main():
    return 'Welcome'

@app.route('/results', methods=['GET', 'POST'])
def results():
    ident = request.form['Ident'].upper()
    if ident == '':
        return redirect(url_for('find_frequencies'))
    results = [
        freqs for freqs in freqs_repo.data if freqs.ident == ident
    ]
    return render_template("results.html", ident=ident, results=results)

@app.route('/find_frequencies')
def find_frequencies():
    return render_template("find_frequencies.html")
