import json
import jsonpickle

class AirportCommRepository:
    def __init__(self, convertedfile_path):
        self.data = jsonpickle.decode(open(convertedfile_path).read())
    
    def find_by_ident(self, ident):
        return [x for x in self.data if ident == x.ident]

