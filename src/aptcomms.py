from src.entities import AirportComm

def _pluck_aptcomm(line):
    return AirportComm(
        ident= line[0:4],
        position= line[5:8],
        freq= float(line[9:17]),
        type= line[17:20],
        lat= float(line[21:30]),
        lng= float(line[32:43]),
        unknow= float(line[44:47])
    )