from src.aptcomms import _pluck_aptcomm
import json
import jsonpickle

def write_airport_freqs(aptcomms_path):
    comms_list = list()
    with open(aptcomms_path) as file:
        lines = [line for line in file.readlines() if not line.startswith(';')]

        for line in lines:
            try:
                comms_list.append(json.loads(jsonpickle.encode(_pluck_aptcomm(line))))
            except Exception as crap:
                raise crap

    with open('data/airport_freqs.json', 'w') as json_file:
        json_file.write(json.dumps(comms_list, indent=4))