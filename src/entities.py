from dataclasses import dataclass

@dataclass()
class AirportComm:
    ident: str
    position: str
    freq: float
    type: str
    lat: float
    lng: float
    unknow: float

@dataclass()
class Nav:
     ident:str
     name:str
     lat: float
     lng: float
     type: str
     freq: float
     loc: str