from src.entities import Nav

def _pluck_nav(line):
    _args = line.split()
    return Nav(
        ident=  _args[0],
        name=   _args[1],
        lat=    float(_args[2]),
        lng=    float(_args[3]), 
        type=   _args[4], 
        freq=   float(_args[5]),
        loc=    _args[6]
    )